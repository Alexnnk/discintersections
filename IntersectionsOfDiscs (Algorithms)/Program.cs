﻿using System;
using System.Linq;

namespace DisksIntersections
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] a = new int[6] { 1, 5, 2, 1, 4, 0 };
            Console.WriteLine(Solution.solution(a));
            
        }
    }
    class Solution
    {
        public static int limit = 10000000;
        // Функция принимает массив значений радиусов окружностей и возвращает количество пар
        // пересекающихся окружностей. Индекс элемента массива соответствует позиции (0,i) центра окружности на плоскости.
        // В случае превышения количества пар значения limit, возвращает -1  
        //   O(N*Log(N)) complexity   //   
        public static int solution(int[] A)
        {
            // Количество пар пересечений
            int intersections = 0;
            // Сортируем левые точки окружностей по возрастанию
            var disks = A
                .Select((x, i) => new { LeftPoint = i - (long)x, RightPoint = i + (long)x })
                .OrderBy(x => x.LeftPoint).ToArray();

            int lenght = disks.Length;
            for (int i = 0; i < lenght - 1; i++)
            {
                int j = i + 1;
                // Для каждой правой точки из массива disks 
                // Пока левая точка следующего элемента массива < правой точки предыдущего - засчитать пересечение
                while (j < lenght && disks[j++].LeftPoint < disks[i].RightPoint)
                {
                    intersections++;
                    if (intersections > limit) return -1;
                }
            }
            return intersections;
        }
    }
}
